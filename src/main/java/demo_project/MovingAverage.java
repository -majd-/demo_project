package demo_project;

public class MovingAverage
{
	PricesRetrieval pr;
	
	public MovingAverage(){
		this.pr = new PricesRetrieval();
	}
	
	public double getAverage(int timeFrame){
		double[] sublist = pr.getSubList(timeFrame);
		int sum = 0;
		for(double num: sublist){
			sum += num;
		}
		
		return sum/sublist.length;
	}
}
