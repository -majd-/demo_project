package demo_project;

import java.util.Arrays;

public class PricesRetrieval
{
	
	

	
	public double[] getSubList(int timeFrame){
	    PricesGenerator pg  = new PricesGenerator();
		double[] historicalData = pg.getDataArray();
		double[] timeFramedData = Arrays.copyOfRange(historicalData, 0, historicalData.length-timeFrame);
		return timeFramedData;
	}

}
